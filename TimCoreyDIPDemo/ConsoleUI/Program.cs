﻿using DemoLibrary;
using System;

namespace ConsoleUI
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("TimCoreyDIPDemoApp!");

            IPerson owner = Factory.CreatePerson();
            //IPerson owner = new Person
            owner.FirstName = "Bernard";
            owner.LastName = "Epp";
            owner.EmailAddress = "test@test.com";
            owner.PhoneNumber = "017464598999";

            IChore chore = Factory.CreateChore();
            chore.ChoreName = "Take out the trash";
            chore.Owner = owner;

            chore.PerformedWork(3);
            chore.PerformedWork(1.5);
            chore.CompleteChore();

            Console.ReadLine();
        }
    }
}
