﻿using DemoLibrary.Utilities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DemoLibrary
{
    public class BetterBusinessLogic : IBusinessLogic
    {
        ILogger _logger;
        IDataAccess _dataAccess;

        public BetterBusinessLogic(ILogger logger, IDataAccess dataAccess)
        {
            _logger = logger;
            _dataAccess = dataAccess;
        }

        public void ProcessData()
        {
            Console.WriteLine();
            _logger.Log("Starting the processing of data.");
            Console.WriteLine();
            Console.WriteLine("Processing of data");

            _dataAccess.LoadData();
            _dataAccess.SaveData("ProcessingInfo");
            Console.WriteLine();
            _logger.Log("Finished processing of the data");
            Console.WriteLine();

        }
    }
}
